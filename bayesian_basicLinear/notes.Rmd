# Setup for Bayesian analysis

1. Select prior
    - Initial believe about parameters $p(\theta)$
    - Independent of data
    - e.g. Normal distribution with prior mean and prior variance selected
2. Select Likelihood
    - PDF of the data given parameters $p(y|\theta)$
    - e.g. Data is Normally distributed
3. Calculate Posterior Density
    - Posterior is likelihood times prior divided by unconditional data density (last ignorable) 
	$$
	p(\theta|y) = \frac{p(y|\theta) p(\theta)}{p(y)} \propto p(y|\theta) p(\theta)
	$$
4. Posterior Inference
    - Usually of the form
	$$
    \mathbf{E}[g(\theta)|y] = \int g(\theta) p(\theta|y)d\theta	
	$$
	
	where $g(\theta)$ is some function. 
