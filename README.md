Repository for any R code you might want to share. Feel free to add your code and raise an issue if something is not working and other people can have a look. 

If somebody violated your copyright please raise an issue and we'll work it out. :)